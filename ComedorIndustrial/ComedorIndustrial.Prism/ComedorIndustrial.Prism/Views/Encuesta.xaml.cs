﻿using Xamarin.Forms;
using ComedorIndustrial.Prism.ViewModels;

namespace ComedorIndustrial.Prism.Views
{
    public partial class Encuesta : ContentPage
    {
        public EncuestaViewModel EncVm;
        public Encuesta()
        {            
            InitializeComponent();
            EncVm= ((EncuestaViewModel)this.BindingContext);
        }

        private void next_Clicked(object sender, System.EventArgs e)
        {
            //var EncVm = EncuestaViewModel.GetInstance();

            int itemcount = EncVm.Encuestas.Detalle.Count;
            var encuestaActual = EncVm.Encuestas.Detalle[CarouselEncuesta.Position];
            if (!string.IsNullOrEmpty(encuestaActual.TextoRespuesta))
            {
                if (CarouselEncuesta.Position + 1 != itemcount)
                {
                    CarouselEncuesta.Position = CarouselEncuesta.Position + 1;
                }
                else
                {
                    //Oculto y desoculto
                    FinalEncuesta();
                }
            }

        }

        private void listView_BindingContextChanged(object sender, System.EventArgs e)
        {
            //var EncVm = EncuestaViewModel.GetInstance();
            int itemcount = EncVm.Encuestas.Detalle.Count;

            if (CarouselEncuesta.Position + 1 != itemcount)
            {
                CarouselEncuesta.Position = CarouselEncuesta.Position + 1;
            }
            else
            {
                //Oculto y desoculto
                FinalEncuesta();
            }

        }

        private void FinalEncuesta()
        {
            StlyCarousel.IsVisible = false;
            StlyFinalEnc.IsVisible = true;
            EncVm.EnviarEncuestas();
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            StlyIntro.IsVisible = false;
            StlyCarousel.IsVisible = true;
        }
    }
}
