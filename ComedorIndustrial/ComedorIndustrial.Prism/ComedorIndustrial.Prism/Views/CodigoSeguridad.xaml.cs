﻿using System;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.Views
{
    public partial class CodigoSeguridad : ContentPage
    {
        public CodigoSeguridad()
        {
            InitializeComponent();
            TxtCode1.TextChanged += (object sender, TextChangedEventArgs e) =>
            {
                if(!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                    TxtCode2.Focus();
            };

            TxtCode2.TextChanged += (object sender, TextChangedEventArgs e) => {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                    TxtCode3.Focus();
            };

            TxtCode3.TextChanged += (object sender, TextChangedEventArgs e) => {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                    TxtCode4.Focus();
            };

            TxtCode4.TextChanged += (object sender, TextChangedEventArgs e) => {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                    TxtCode5.Focus();
            };

        }
    }
}
