﻿using ComedorIndustrial.Prism.ViewModels;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.Views
{
    public partial class MainTabbedPage : TabbedPage
    {
        private MainTabbedPageViewModel viewModel;
        public MainTabbedPage()
        {
            InitializeComponent();
            viewModel = this.BindingContext as MainTabbedPageViewModel;
            viewModel.navigationBarSet = SetNavigagionBarVisible;
        }

        private void SetNavigagionBarVisible(bool visible)
        {
            NavigationPage.SetHasNavigationBar(this, visible);
        }
    }
}
