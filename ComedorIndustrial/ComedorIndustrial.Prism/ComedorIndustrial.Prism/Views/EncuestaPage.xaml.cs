﻿using ComedorIndustrial.Prism.ViewModels;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.Views
{
    public partial class EncuestaPage : ContentPage
    {
        public EncuestaPageViewModel EncVm;
        public EncuestaPage()
        {
            InitializeComponent();
            EncVm = ((EncuestaPageViewModel)this.BindingContext);
        }

        private void next_Clicked(object sender, System.EventArgs e)
        {
            //var EncVm = EncuestaViewModel.GetInstance();

            int itemcount = EncVm.Encuestas.Detalle.Count;
            var encuestaActual = EncVm.Encuestas.Detalle[CarouselEncuesta.Position];
            if (!string.IsNullOrEmpty(encuestaActual.TextoRespuesta))
            {
                if (CarouselEncuesta.Position + 1 != itemcount)
                {
                    CarouselEncuesta.Position = CarouselEncuesta.Position + 1;
                }
                else
                {
                    //Oculto y desoculto
                    FinalEncuesta();
                }
            }

        }

        private void listView_BindingContextChanged(object sender, System.EventArgs e)
        {
            //var EncVm = EncuestaViewModel.GetInstance();
            int itemcount = EncVm.Encuestas.Detalle.Count;

            if (CarouselEncuesta.Position + 1 != itemcount)
            {
                CarouselEncuesta.Position = CarouselEncuesta.Position + 1;
            }
            else
            {
                //Oculto y desoculto
                FinalEncuesta();
            }

        }

        private void FinalEncuesta()
        {
            StlyFinalEnc.IsVisible = true;
            EncVm.EnviarEncuestas();
        }

    }
}
