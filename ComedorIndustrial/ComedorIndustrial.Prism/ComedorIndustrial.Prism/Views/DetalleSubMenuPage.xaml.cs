﻿using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.ViewModels;
using Syncfusion.ListView.XForms;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.Views
{
    public partial class DetalleSubMenuPage : ContentPage
    {
        DetalleSubMenuPageViewModel vm;
        public DetalleSubMenuPage()
        {
            InitializeComponent();
            vm = this.BindingContext as DetalleSubMenuPageViewModel;
        }

        private void listView_SelectionChanging(object sender, ItemSelectionChangingEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                if (e.RemovedItems.Count > 0)
                {
                    var opcionActual = (e.RemovedItems[e.RemovedItems.Count - 1] as DetalleSubMenu);
                    opcionActual.Seleccionado = false;
                    e.RemovedItems.Clear();
                }
            }
            else
            {
                var opcionActual = (e.AddedItems[e.AddedItems.Count - 1] as DetalleSubMenu);
                var _grupo = vm.GrupoSubMenu.Where(w => w.Id == opcionActual.Grupo).FirstOrDefault();
                int maxSeleccion = _grupo.Encabezado.Marcas;
                int seleccionados = _grupo.Opciones.Where(w => w.Seleccionado == true).Count();

                if (seleccionados >= maxSeleccion)
                {
                    e.Cancel = true;
                    return;
                }

                opcionActual.Seleccionado = true;
            }
        }

        private void CheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }
    }
}
