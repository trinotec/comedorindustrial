﻿using Prism.Services.Dialogs;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.Helper
{
    public static class CustomDialog
    {
        public static void ShowAlert(this IDialogService dialogService, string title, string message, string type = "error")
        {
            string textButton;
            ImageSource icon;
            Color bgcolor;

            if(type == "success")
            {
                textButton = "Ok";
                icon = ImageSource.FromFile("iconoSuccess.png");
                bgcolor = Color.FromHex("2ee2b1");
            }
            else
            {
                textButton = "Volver a Intentar";
                icon = ImageSource.FromFile("iconoError.png");
                bgcolor = Color.FromHex("ee3837");
            }

           var parameters = new DialogParameters
            {
                { "title", title },
                { "message", message },
                { "icon", icon },
                { "bgcolor", bgcolor },
                { "textbutton", textButton },
            };
            dialogService.ShowDialog("MessageDialog", parameters);
        }
    }
}
