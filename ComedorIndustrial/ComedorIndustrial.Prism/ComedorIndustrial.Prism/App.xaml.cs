﻿using Prism;
using Prism.Ioc;
using ComedorIndustrial.Prism.ViewModels;
using ComedorIndustrial.Prism.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ComedorIndustrial.Prism.Services;
using Prism.Modularity;
using System;
using ComedorIndustrial.Prism.Helper;
using Prism.Plugin.Popups;
using System.Globalization;
using Xamarin.Essentials;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ComedorIndustrial.Prism
{
    public partial class App
    {
        const int smallHeightResolution = 1280;
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjM1NTQ2QDMxMzgyZTMxMmUzMExXMHdoa0wyNklhL2RMUHJoY3JxL0dJNnBHRzN5M0ZacDZ0dEd0UWRHUFU9");
            CultureInfo myCurrency = new CultureInfo("es-CR"); CultureInfo.DefaultThreadCurrentCulture = myCurrency;

            InitializeComponent();
            LoadStyles();

            if (Settings.IsLogin == true)
            {
                await NavigationService.NavigateAsync("/MainMasterDetailPage/NavigationPage/MainTabbedPage");                             
            }
            else
            {
                await NavigationService.NavigateAsync("/LoginPage");
            }
        }

        void LoadStyles()
        {
            if (IsASmallDevice())
            {
                dictionary.MergedDictionaries.Add(SmallDevicesStyle.SharedInstance);
            }
            else
            {
                dictionary.MergedDictionaries.Add(GeneralDevicesStyle.SharedInstance);
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterPopupDialogService();
            containerRegistry.Register<IApiService, ApiService>();
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<RecuperarContrasena, RecuperarContrasenaViewModel>();
            containerRegistry.RegisterForNavigation<CodigoSeguridad, CodigoSeguridadViewModel>();
            containerRegistry.RegisterForNavigation<CambiarContrasena, CambiarContrasenaViewModel>();
            containerRegistry.RegisterForNavigation<MainMasterDetailPage, MainMasterDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<MainTabbedPage, MainTabbedPageViewModel>();
            containerRegistry.RegisterForNavigation<FacturasGralPage, FacturasGralPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfilePage, ProfilePageViewModel>();
            containerRegistry.RegisterForNavigation<NoticiasPage, NoticiasPageViewModel>();
            containerRegistry.RegisterForNavigation<NoticiaDetallePage, NoticiaDetallePageViewModel>();

            containerRegistry.RegisterDialog<MessageDialog, MessageDialogViewModel>();

            containerRegistry.RegisterForNavigation<Encuesta, EncuestaViewModel>();
            containerRegistry.RegisterForNavigation<FechasFacturasPage, FechasFacturasPageViewModel>();
            containerRegistry.RegisterForNavigation<MonederoPage, MonederoPageViewModel>();
            containerRegistry.RegisterForNavigation<FechaMonederoPage, FechaMonederoPageViewModel>();
            containerRegistry.RegisterForNavigation<EncuestaPage, EncuestaPageViewModel>();
            containerRegistry.RegisterForNavigation<CategoriaPage, CategoriaPageViewModel>();
            containerRegistry.RegisterForNavigation<EmptyCarPage, EmptyCarPageViewModel>();
            containerRegistry.RegisterForNavigation<CarPage, CarPageViewModel>();
            containerRegistry.RegisterForNavigation<DetalleSubMenuPage, DetalleSubMenuPageViewModel>();
            containerRegistry.RegisterForNavigation<PreparandoPage, PreparandoPageViewModel>();
        }

        public static bool IsASmallDevice()
        {
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
            var height = mainDisplayInfo.Height;
            return (height <= smallHeightResolution);
        }
    }
}
