﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class TransaccionConsumoDetallado
    {
        public List<MovimientoDetallado> movimientos { get; set; }
        public double totalSubsidio { get; set; }
        public double totalCredito { get; set; }
        public double totalOtros { get; set; }
        public double totalGeneralFacturas { get; set; }
    }
}
