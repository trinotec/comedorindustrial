﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class CambiarContrasenaRequest
    {
        public string CodCliente { get; set; }
        public string Clave { get; set; }
    }
}
