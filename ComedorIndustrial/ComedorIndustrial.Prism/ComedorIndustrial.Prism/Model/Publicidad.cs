﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Publicidad
    {
        public int CodPublicidad { get; set; }
        public string Descripcion { get; set; }
        public string Url { get; set; }
    }
}
