﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class UserResponse
    {
        public bool Success { get; set; }
        public string Nombre { get; set; }
        public string Imagen { get; set; }
    }
}
