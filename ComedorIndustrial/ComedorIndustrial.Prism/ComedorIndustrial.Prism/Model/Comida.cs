﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Comida
    {
        public int CodBanner { get; set; }
        public string Nombre { get; set; }
        public string url { get; set; }
        public bool Activo { get; set; }
        public bool ImagenMenu { get; set; }
        public string Tipo { get; set; }
        public int Dia { get; set; }
        public int Semana { get; set; }
        public string Calorias { get; set; }
        public string url2 { get; set; }
        public string Acompanamiento { get; set; }
        public string DiaNombre { get => GetDia(this.Dia); }

        private string GetDia(int dia)
        {
            string dayName = "";
            switch (dia)
            {
                case 1:
                    dayName = "Lunes";
                    break;
                case 2:
                    dayName = "Martes";
                    break;
                case 3:
                    dayName = "Miércoles";
                    break;
                case 4:
                    dayName = "Jueves";
                    break;
                case 5:
                    dayName = "Viernes";
                    break;
                case 6:
                    dayName = "Sábado";
                    break;
                case 7:
                    dayName = "Domingo";
                    break;
            }

            return dayName;
        }

    }
}
