﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class TransaccionRequest
    {
        public string CodCliente { get; set; }
        public string fechaInicial { get; set; }
        public string fechaFinal { get; set; }
    }
}
