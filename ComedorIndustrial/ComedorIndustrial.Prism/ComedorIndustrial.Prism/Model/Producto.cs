﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Producto
    {
        public string CodCla { get; set; }
        public int NumMenu { get; set; }
        public string Codigo { get; set; }
        public string DescripcionCorta { get; set; }
        public string DescripcionLarga { get; set; }
        public string TipoMenu { get; set; }
        public float PreVta { get; set; }
        public string UrlFoto { get; set; }
        //public object[] SubMenu { get; set; }

    }
}
