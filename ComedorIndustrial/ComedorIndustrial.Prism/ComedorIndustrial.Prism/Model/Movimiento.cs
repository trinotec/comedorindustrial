﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Movimiento
    {
        public DateTime Fecha { get; set; }
        public int TipoTransaccion { get; set; }
        public double Monto { get; set; }
    }
}
