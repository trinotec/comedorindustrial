﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class MovimientoDetallado
    {
        public int NumFactura { get; set; }
        public DateTime Fecha { get; set; }
        public int TipoTransaccion { get; set; }
        public double Subsidio { get; set; }
        public double Credito { get; set; }
        public double Otros { get; set; }
        public double TotalFactura { get; set; }
        public bool MostrarDetalle { get; set; }
        public double Monedero { get; set; }
        public int HeightRequest { get; set; }
        public List<DetalleMovimiento> Detalle { get; set; }
    }
}
