﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class DetalleMovimiento
    {
        public string NomPro { get; set; }
        public double Cantidad { get; set; }
        public double PrecioUnitario { get; set; }
        public double TotalLinea { get; set; }
    }
}
