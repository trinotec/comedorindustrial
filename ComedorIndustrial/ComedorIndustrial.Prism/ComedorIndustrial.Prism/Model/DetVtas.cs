﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class DetVtas
    {
        public int NumLinea { get; set; }
        public string CodPro { get; set; }
        public string NomPro { get; set; }
        public float PreVenta { get; set; }
        public int PorDescto { get; set; }
        public int uniVen { get; set; }
        public string Comentarios { get; set; }
        public bool Principal { get; set; }
    }

}
