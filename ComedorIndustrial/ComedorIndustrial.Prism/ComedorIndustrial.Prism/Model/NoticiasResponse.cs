﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class NoticiasResponse
    {
        public ICollection<Noticia> NoticiasMasLeidas { get; set; }
        public ICollection<Noticia> NoticiasMasRecientes { get; set; }
    }
}
