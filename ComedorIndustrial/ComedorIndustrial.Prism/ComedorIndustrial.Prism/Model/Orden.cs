﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Venta : RealmObject
    {
        [PrimaryKey]
        public int Id { get; set; }
        public float ImporteVenta { get; set; }
        public int CantidadVenta { get; set; }
        public bool RequirePajilla { get; set; }

        public IList<DetalleVenta> DetalleVentas { get; }
    }

    public class DetalleVenta : RealmObject {
        public string CodCla { get; set; }
        public int NumMenu { get; set; }
        public int NumItem { get; set; }
        public string Leyenda { get; set; }
        public string DescripcionLarga { get; set; }
        public string TipoMenu { get; set; }
        public string Codigo { get; set; }
        public float PreVta { get; set; }
        public int Marcas { get; set; }
        public bool EsPrincipal { get; set; }
        public int TotalOrdenados { get; set; }
        public string Comentarios { get; set; }

    }
}
