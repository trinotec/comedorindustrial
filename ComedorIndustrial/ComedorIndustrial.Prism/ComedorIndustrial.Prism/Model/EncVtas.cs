﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class EncVtas
    {
        public string TipoFactura { get; set; }
        public string CodCliente { get; set; }
        public string NomCliente { get; set; }
        public string CodRest { get; set; }
        public float Subtotal { get; set; }
        public float Descuento { get; set; }
        public float ImptoVentas { get; set; }
        public float FleteExpress { get; set; }
        public string NumTar { get; set; }
        public float MtoTar { get; set; }
        public string NumReferencia { get; set; }
        public string NumAutorizacion { get; set; }
        public string NumTarjetaCF { get; set; }

    }
}
