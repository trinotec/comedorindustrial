﻿using ComedorIndustrial.Prism.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class DetalleSubMenu : BindableBase
    {
        public int NumMenu { get; set; }
        public int NumItem { get; set; }
        public string Leyenda { get; set; }
        public string TipoMenu { get; set; }
        public string Codigo { get; set; }
        public decimal PreVta { get; set; }
        public int Marcas { get; set; }
        public bool Obligatorio { get; set; }

        public int Grupo { get; set; }

        private int _total;
        public int Total
        {
            get => _total;
            set 
            {
                CheckIncrement(value);
                SetProperty(ref _total, value);
            }
        }

        private bool _seleccionado;
        public bool Seleccionado
        {
            get => _seleccionado;
            set {
                SetProperty(ref _seleccionado, value);
                DetalleSubMenuPageViewModel.GetInstance().CheckComplete();
            }
        }

        private DelegateCommand _incrementTotalCommand;
        public DelegateCommand IncrementTotalCommand => _incrementTotalCommand ?? (_incrementTotalCommand = new DelegateCommand(IncrementTotal));

        private DelegateCommand _decrementTotalCommand;
        public DelegateCommand DecrementTotalCommand => _decrementTotalCommand ?? (_decrementTotalCommand = new DelegateCommand(DecrementTotal));

        private void IncrementTotal()
        {
            Total++;
        }

        private void DecrementTotal()
        {
            Total--;
        }

        private void CheckIncrement(int newValue)
        {
            var diferencia = newValue - Total;
            if (diferencia != 0)
            {
                var TotalDiferido = PreVta * diferencia;
                DetalleSubMenuPageViewModel.GetInstance().TotalOrden += (float)TotalDiferido * DetalleSubMenuPageViewModel.GetInstance().CantidadOrden;
            }
        }
    }
}
