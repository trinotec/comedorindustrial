﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class ResultStatusCode
    {
        public bool success { get; set; }
        public string message { get; set; }
    }   
}
