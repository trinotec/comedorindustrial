﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class UserRequest : Cliente
    {
        public string ImagenBase64 { get; set; }
        public bool Activo { get; set; }
    }
}
