﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Detalle
    {
        public string Descripcion { get; set; }
        public int IdPregunta { get; set; }
        public List<Opinion> Opiniones { get; set; }
    }
}
