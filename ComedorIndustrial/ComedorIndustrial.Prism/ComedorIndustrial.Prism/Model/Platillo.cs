﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class Platillo
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Imagen { get; set; }
    }
}
