﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class VentaRequest
    {
        public EncVtas encVtas { get; set; }
        public List<DetVtas> detVtas { get; set; }
    }
}
