﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class LoginRequest
    {
        public string codigo { get; set; }
        public string pw { get; set; }
    }
}
