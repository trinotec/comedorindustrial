﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public  class GrupoSubMenu
    {
        public int Id { get; set; }
        public DetalleSubMenu Encabezado { get; set; }
        public ICollection<DetalleSubMenu> Opciones { get; set; }
    }
}
