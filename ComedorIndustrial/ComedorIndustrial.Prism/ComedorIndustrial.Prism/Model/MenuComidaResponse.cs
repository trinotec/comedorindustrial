﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class MenuComidaResponse
    {
        public ICollection<Comida> Dia { get; set; }
        public ICollection<Comida> Semana { get; set; }
        public ICollection<Comida> Chef { get; set; }
        public ICollection<Comida> Combo { get; set; }
        public ICollection<Comida> Reposteria { get; set; }
        public List<Publicidad> Publicidad { get; set; }
    }
}
