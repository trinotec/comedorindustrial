﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class DesgloseVenta
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public float Precio { get; set; }
    }
}
