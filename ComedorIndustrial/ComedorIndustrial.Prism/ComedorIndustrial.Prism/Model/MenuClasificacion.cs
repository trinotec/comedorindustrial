﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class MenuClasificacion : BindableBase
    {
        private string _codCla;
        public string CodCla
        {
            get => _codCla;
            set => SetProperty(ref _codCla, value);
        }
        public string NomCla { get; set; }
        public string UrlFoto { get; set; }
        public bool Activo { get; set; }

        private double _opacidad;
        public double Opacidad
        {
            get => _opacidad;
            set => SetProperty(ref _opacidad, value);
        }
    }
}
