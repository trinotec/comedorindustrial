﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.Model
{
    public class TransaccionMonedero
    {

        public double montoAcumulado { get; set; }
        public double montoRedimido { get; set; }
        public double saldoActual { get; set; }
        public double saldoInicial { get; set; }
        public List<Movimiento> movimientos { get; set; }
    }
}
