﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism
{
    public class NoShiftEffect : RoutingEffect
    {
        public NoShiftEffect() : base("MyCompany.NoShiftEffect")
        {

        }
    }
}
