﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ComedorIndustrial.Prism.Model;
using Newtonsoft.Json;

namespace ComedorIndustrial.Prism.Services
{
    public class ApiService : IApiService
    {
        public async Task<Response<LoginResponse>> GetLoginAsync(string urlBase, string servicePrefix, string controller, LoginRequest request)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("codigo={0}&pw={1}", request.codigo, request.pw);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<LoginResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var token = JsonConvert.DeserializeObject<LoginResponse>(result);
                return new Response<LoginResponse>
                {
                    IsSuccess = true,
                    Result = token
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<LoginResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<MenuComidaResponse>> GetMenuByCategoria(string urlBase, string servicePrefix, string controller)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<MenuComidaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var _data = JsonConvert.DeserializeObject<MenuComidaResponse>(result);
                return new Response<MenuComidaResponse>
                {
                    IsSuccess = true,
                    Result = _data
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<MenuComidaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<CodeRecuperacionResponse>> GetCodeRecuperacionAsync(string urlBase, string servicePrefix, string controller, CodeRecuperacionRequest request)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("codigo={0}", request.codigo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<CodeRecuperacionResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var token = JsonConvert.DeserializeObject<CodeRecuperacionResponse>(result);
                return new Response<CodeRecuperacionResponse>
                {
                    IsSuccess = true,
                    Result = token
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<CodeRecuperacionResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<CambiarContrasenaResponse>> UpdateContrasena(string urlBase, string servicePrefix, string controller, CambiarContrasenaRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PutAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<CambiarContrasenaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<CambiarContrasenaResponse>(result);
                return new Response<CambiarContrasenaResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<CambiarContrasenaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<UserResponse>> UpdateProfile(string urlBase, string servicePrefix, string controller, UserRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PutAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<UserResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<UserResponse>(result);
                return new Response<UserResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<UserResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<NoticiasResponse>> GetListNoticias(string urlBase, string servicePrefix, string controller)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                //UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<NoticiasResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var noticias = JsonConvert.DeserializeObject<NoticiasResponse>(result);
                return new Response<NoticiasResponse>
                {
                    IsSuccess = true,
                    Result = noticias
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<NoticiasResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }


        public async Task<Response<List<Encuesta>>> GetEncuesta(string urlBase, string servicePrefix, string controller)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    var resultadoStatus = JsonConvert.DeserializeObject<ResultStatusCode>(result);
                    return new Response<List<Encuesta>>
                    {
                        IsSuccess = false,
                        Message = resultadoStatus.message                        
                    };
                }

                var Encuestas = JsonConvert.DeserializeObject<List<Encuesta>>(result);
                return new Response<List<Encuesta>>
                {
                    IsSuccess = true,
                    Result = Encuestas
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<Encuesta>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<EncuestaResponse>> SendResultEncuesta(string urlBase, string servicePrefix, string controller, List<EncuestaRequest> request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<EncuestaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<EncuestaResponse>(result);
                return new Response<EncuestaResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<EncuestaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }


        public async Task<Response<TransaccionConsumoDetallado>> GetTransaccionesDetalle(string urlBase, string servicePrefix, string controller, TransaccionRequest request)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("CodCliente={0}&fechaInicial={1}&fechaFinal={2}", request.CodCliente, request.fechaInicial,request.fechaFinal);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<TransaccionConsumoDetallado>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var transacciones = JsonConvert.DeserializeObject<TransaccionConsumoDetallado>(result);
                return new Response<TransaccionConsumoDetallado>
                {
                    IsSuccess = true,
                    Result = transacciones
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<TransaccionConsumoDetallado>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<TransaccionMonedero>> GetTransaccionesMonedero(string urlBase, string servicePrefix, string controller, TransaccionRequest request)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("CodCliente={0}&fechaInicial={1}&fechaFinal={2}", request.CodCliente, request.fechaInicial, request.fechaFinal);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<TransaccionMonedero>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var transacciones = JsonConvert.DeserializeObject<TransaccionMonedero>(result);
                return new Response<TransaccionMonedero>
                {
                    IsSuccess = true,
                    Result = transacciones
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<TransaccionMonedero>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<List<MenuClasificacion>>> GetCategoriasMenu(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    var resultadoStatus = JsonConvert.DeserializeObject<ResultStatusCode>(result);
                    return new Response<List<MenuClasificacion>>
                    {
                        IsSuccess = false,
                        Message = resultadoStatus.message
                    };
                }

                var Categorias = JsonConvert.DeserializeObject<List<MenuClasificacion>>(result);
                return new Response<List<MenuClasificacion>>
                {
                    IsSuccess = true,
                    Result = Categorias
                };

            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<MenuClasificacion>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<List<Producto>>> GetProductosByCategoria(string urlBase, string servicePrefix, string controller, string codCla)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("CodCla={0}", codCla);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    var resultadoStatus = JsonConvert.DeserializeObject<ResultStatusCode>(result);
                    return new Response<List<Producto>>
                    {
                        IsSuccess = false,
                        Message = resultadoStatus.message
                    };
                }

                var Productos = JsonConvert.DeserializeObject<List<Producto>>(result);
                return new Response<List<Producto>>
                {
                    IsSuccess = true,
                    Result = Productos
                };

            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<Producto>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<List<DetalleSubMenu>>> GetDetalleSubMenu(string urlBase, string servicePrefix, string controller, int codCla)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("CodMenu={0}", codCla);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    var resultadoStatus = JsonConvert.DeserializeObject<ResultStatusCode>(result);
                    return new Response<List<DetalleSubMenu>>
                    {
                        IsSuccess = false,
                        Message = resultadoStatus.message
                    };
                }

                var Productos = JsonConvert.DeserializeObject<List<DetalleSubMenu>>(result);
                return new Response<List<DetalleSubMenu>>
                {
                    IsSuccess = true,
                    Result = Productos
                };

            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<DetalleSubMenu>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<EncuestaResponse>> SendVenta(string urlBase, string servicePrefix, string controller, VentaRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<EncuestaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<EncuestaResponse>(result);
                return new Response<EncuestaResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<EncuestaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }
    }
}
