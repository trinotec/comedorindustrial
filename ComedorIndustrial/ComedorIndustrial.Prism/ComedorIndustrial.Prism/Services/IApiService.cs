﻿using ComedorIndustrial.Prism.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ComedorIndustrial.Prism.Services
{
    public interface IApiService
    {
        Task<Response<LoginResponse>> GetLoginAsync(
            string urlBase,
            string servicePrefix,
            string controller,
            LoginRequest request
            );

        Task<Response<MenuComidaResponse>> GetMenuByCategoria(
            string urlBase,
            string servicePrefix,
            string controller
            );

            Task<Response<CodeRecuperacionResponse>> GetCodeRecuperacionAsync(
        string urlBase,
        string servicePrefix,
        string controller,
        CodeRecuperacionRequest request
     );


        Task<Response<CambiarContrasenaResponse>> UpdateContrasena(
            string urlBase,
            string servicePrefix,
            string controller,
            CambiarContrasenaRequest request
         );

        Task<Response<UserResponse>> UpdateProfile(
            string urlBase,
            string servicePrefix,
            string controller,
            UserRequest request
         );

        Task<Response<NoticiasResponse>> GetListNoticias(
            string urlBase,
            string servicePrefix,
            string controller
         );

        Task<Response<List<Encuesta>>> GetEncuesta(
           string urlBase,
           string servicePrefix,
           string controller
        );

        Task<Response<EncuestaResponse>> SendResultEncuesta(
            string urlBase,
            string servicePrefix,
            string controller,
            List<EncuestaRequest> request
         );


        Task<Response<TransaccionConsumoDetallado>> GetTransaccionesDetalle(
            string urlBase,
            string servicePrefix,
            string controller,
            TransaccionRequest request
         );

        Task<Response<TransaccionMonedero>> GetTransaccionesMonedero(
            string urlBase,
            string servicePrefix,
            string controller,
            TransaccionRequest request
         );

        Task<Response<List<MenuClasificacion>>> GetCategoriasMenu(
           string urlBase,
           string servicePrefix,
           string controller
        );

        Task<Response<List<Producto>>> GetProductosByCategoria(
           string urlBase,
           string servicePrefix,
           string controller,
           string codCla
        );

        Task<Response<List<DetalleSubMenu>>> GetDetalleSubMenu(
           string urlBase,
           string servicePrefix,
           string controller,
           int codCla
        );

        Task<Response<EncuestaResponse>> SendVenta(
            string urlBase,
            string servicePrefix,
            string controller,
            VentaRequest request
         );


    }
}
