﻿using ComedorIndustrial.Prism.Model;
using Xamarin.Forms;
namespace ComedorIndustrial.Prism.Controls
{
    class DetalleSubMenuTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CheckboxDataTemplate { get; set; }
        public DataTemplate IncrementalDataTemplate { get; set; }
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((GrupoSubMenu)item).Encabezado.Marcas == 0 ? IncrementalDataTemplate : CheckboxDataTemplate;
        }
    }
}
