﻿using ComedorIndustrial.Prism.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.Controls
{
    class EncuestaTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PreguntaAbierta { get; set; }
        public DataTemplate PreguntaCerrada { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            DataTemplate template;
            template = ((DetallaEncViewModel)item).DetallePreguntaAbierta == true ? PreguntaAbierta : PreguntaCerrada;
            return template;
        }
    }
}
