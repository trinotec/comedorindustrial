﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComedorIndustrial.Prism.Model;
using Prism.Navigation;
using ComedorIndustrial.Prism.Services;
using Prism.Services.Dialogs;
using System.Collections.ObjectModel;
 

namespace ComedorIndustrial.Prism.ViewModels
{
    
    public class NoticiaDetallePageViewModel : ViewModelBase
    {
        private Noticia _noticia;
        private bool _isRunning;
        private bool _isVisible;

        public Noticia Noticia
        {
            get => _noticia;
            set => SetProperty(ref _noticia, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        public NoticiaDetallePageViewModel(INavigationService navigationService) : base(navigationService)
        {
            IsRunning = true;
            IsVisible = false;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("noticia"))
            {
                Noticia = parameters.GetValue<Noticia>("noticia");
            }
        }

        private async void LoadData()
        {
            IsRunning = true;
            IsVisible = false;
            Noticia noticiaTemp = new Noticia();


            try
            {

                noticiaTemp = new Noticia()
                {
                    Titulo = "\"Comer Saludable no es caro\"",
                    Fecha = new DateTime(2019, 5, 1),
                    url = "https://i.ibb.co/YLrKfLY/Image-29.png",
                    Autor = "Karla Giraldo",
                    Detalle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor metus sit " +
                    "amet urna imperdiet euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed urna orci, tincidunt in rutrum ac, " +
                    "viverra in erat. Etiam et lorem vel risus bibendum porta eu ut ante. In massa nisl, ullamcorper nec felis sit amet, consectetur placerat " +
                    "sapien. Donec vulputate convallis est et euismod. In urna magna, dapibus vitae tincidunt non, consectetur quis orci. In hac habitasse platea " +
                    "dictumst. Donec sit amet mollis erat. In iaculis, lectus sit amet luctus rhoncus, arcu diam pulvinar tellus, sed laoreet arcu arcu eu nibh. "

                };

                Noticia = noticiaTemp;

                await Task.Delay(500);
                IsRunning = false;
                IsVisible = true;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }


    }
}
