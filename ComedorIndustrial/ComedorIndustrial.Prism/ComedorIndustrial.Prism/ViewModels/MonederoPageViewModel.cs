﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.Services;
using Newtonsoft.Json;
using Prism;
using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class MonederoPageViewModel : ViewModelBase, INavigationAware, IPageLifecycleAware, IActiveAware
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        public event EventHandler IsActiveChanged;
        private DelegateCommand _navigateFechas;
        private DateTime _fechaInicio;
        private DateTime _fechaFin;
        private string _txtFechasRango;
        private string _descripcionMovimiento;
        private static MonederoPageViewModel _instance;

        private bool _isRunningMonedero;

        public bool IsRunningMonedero
        {
            get => _isRunningMonedero;
            set => SetProperty(ref _isRunningMonedero, value);
        }

        public string TxtFechasRango
        {
            get => _txtFechasRango;
            set => SetProperty(ref _txtFechasRango, value);
        }

        public string DescripcionMovimiento
        {
            get => _descripcionMovimiento;
            set => SetProperty(ref _descripcionMovimiento, value);
        }

        private TransaccionMonedero _transacciones;
        public TransaccionMonedero Transacciones
        {
            get => _transacciones;
            set => SetProperty(ref _transacciones, value);
        }

        private ObservableCollection<MovimientoViewModel> _movimientos;
        public ObservableCollection<MovimientoViewModel> Movimientos
        {
            get => _movimientos;
            set => SetProperty(ref _movimientos, value);
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { SetProperty(ref _isActive, value, RaiseIsActiveChanged); }
        }

        protected virtual void RaiseIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);
        }

        public DelegateCommand CommandFechas => _navigateFechas ?? (_navigateFechas = new DelegateCommand(ExecuteModalFechas));
        public MonederoPageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            _fechaInicio = DateTime.Now.AddDays(-30);
            _fechaFin = DateTime.Now;
            DescripcionMovimiento = "Movimientos Ultimo Mes";
            _instance = this;

            //var tabbpage = MainTabbedPageViewModel.GetInstance();
            //tabbpage.Title = "Estado";

        }

        public void ExecuteModalFechas()
        {
            _navigationService.NavigateAsync("FechaMonederoPage", useModalNavigation: true);
        }

        public static MonederoPageViewModel GetInstance()
        {
            return _instance;
        }

        public async void Load()
        {
            SetTxtFechaRango();
            IsRunningMonedero = true;

            try
            {
                var cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
                var transaccionRequest = new TransaccionRequest
                {
                    CodCliente = cliente.CodCliente,                                 
                    fechaInicial = _fechaInicio.ToString("MM/dd/yyyy"),
                    fechaFinal = _fechaFin.ToString("MM/dd/yyyy")
                };

                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetTransaccionesMonedero(url, _app, "/Transacciones", transaccionRequest);
                if (!response.IsSuccess)
                {
                    IsRunningMonedero = false;
                    CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }
                
                Transacciones = response.Result;
                if (response.Result.movimientos.Count > 0)
                {
                    Movimientos = new ObservableCollection<MovimientoViewModel>(
                        response.Result.movimientos.Select(
                        l => new MovimientoViewModel()
                        {
                              Fecha = l.Fecha,
                              Monto = l.Monto,
                              TipoTransaccion = l.TipoTransaccion
                        })
                        .ToList()
                        );
                }
                else
                {
                    Movimientos = new ObservableCollection<MovimientoViewModel>();
                }

                IsRunningMonedero = false;

            }
            catch (Exception e)
            {
                IsRunningMonedero = false;
                var msg = e.Message;
            }
        }

        public void SetTxtFechaRango()
        {
            TxtFechasRango = "Del " + _fechaInicio.ToString("dd/MM/yy") + " al " + _fechaFin.ToString("dd/MM/yy");
        }

        public void UpdateFacturas(DateTime fechaInicioAct, DateTime fechaFinAct)
        {
            DescripcionMovimiento = "Movimientos";
            _fechaInicio = fechaInicioAct;
            _fechaFin = fechaFinAct;
            Load();
        }

        public void OnAppearing()
        {
            MainTabbedPageViewModel.GetInstance().Title = "Movimientos";
            MainTabbedPageViewModel.GetInstance().SetBarNavigation(true);
            Load();
        }

        public void OnDisappearing()
        {
            Debug.WriteLine("MyTabbedPage is disappearing");
        }
    }
}
