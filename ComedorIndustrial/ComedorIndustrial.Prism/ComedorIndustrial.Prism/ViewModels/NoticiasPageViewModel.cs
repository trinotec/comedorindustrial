﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Services;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class NoticiasPageViewModel : ViewModelBase
    {

        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private bool _isRunning;
        private bool _isVisible;

        private ObservableCollection<NoticiaItemViewModel> _noticiasMasleidas;
        private ObservableCollection<NoticiaItemViewModel> _noticiasRecientes;


        public ObservableCollection<NoticiaItemViewModel> NoticiasMasLeidas
        {
            get => _noticiasMasleidas;
            set => SetProperty(ref _noticiasMasleidas, value);
        }

        public ObservableCollection<NoticiaItemViewModel> NoticiasRecientes
        {
            get => _noticiasRecientes;
            set => SetProperty(ref _noticiasRecientes, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }


        public NoticiasPageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            IsRunning = true;
            IsVisible = false;
        }


        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.New)
            {
                LoadDataNoticias();
            }
        }


        private async void LoadDataNoticias()
        {
            IsRunning = true;
            IsVisible = false;

            try
            {
                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetListNoticias(url, _app, "/Noticias");
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }

                var _data = response.Result;

                NoticiasMasLeidas = new ObservableCollection<NoticiaItemViewModel>(
                    _data.NoticiasMasLeidas.Select(n => new NoticiaItemViewModel(_navigationService) 
                    {
                        Autor = n.Autor,
                        Fecha = n.Fecha,
                        Titulo = n.Titulo,
                        Detalle = n.Detalle,
                        FechaCreacion = n.FechaCreacion,
                        FechaPublicacion = n.FechaPublicacion,
                        IdNoticia = n.IdNoticia,
                        url = n.url
                    }).ToList());

                NoticiasRecientes = new ObservableCollection<NoticiaItemViewModel>(
                    _data.NoticiasMasRecientes.Select(n => new NoticiaItemViewModel(_navigationService)
                    {
                        Autor = n.Autor,
                        Fecha = n.Fecha,
                        Titulo = n.Titulo,
                        Detalle = n.Detalle,
                        FechaCreacion = n.FechaCreacion,
                        FechaPublicacion = n.FechaPublicacion,
                        IdNoticia = n.IdNoticia,
                        url = n.url
                    }).ToList());


                await Task.Delay(1000);
                IsRunning = false;
                IsVisible = true;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }

    }
}
