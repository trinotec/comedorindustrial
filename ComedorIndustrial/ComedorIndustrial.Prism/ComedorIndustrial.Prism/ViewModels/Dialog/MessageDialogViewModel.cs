﻿using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class MessageDialogViewModel : BindableBase, IDialogAware, IAutoInitialize
    {
        private string _title = "Alert";
        private string _message;
        private Color _bgcolor;
        private string _textButton;
        private ImageSource _icon;

        public MessageDialogViewModel()
        {
            CloseCommand = new DelegateCommand(() => RequestClose(null));
        }

        
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public Color BgColor
        {
            get => _bgcolor;
            set => SetProperty(ref _bgcolor, value);
        }

        public string TextButton
        {
            get => _textButton;
            set => SetProperty(ref _textButton, value);
        }

        public ImageSource Icon
        {
            get => _icon;
            set => SetProperty(ref _icon, value);
        }


        [AutoInitialize(true)]
        public string Message
        {
            get => _message;
            set => SetProperty(ref _message, value);
        }

        public DelegateCommand CloseCommand { get; }

        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed()
        {
            Console.WriteLine("The Demo Dialog has been closed...");
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            // If not using IAutoInitialize you would need to set the Message property here.
            // Message = parameters.GetValue<string>("message");
        }
    }
}
