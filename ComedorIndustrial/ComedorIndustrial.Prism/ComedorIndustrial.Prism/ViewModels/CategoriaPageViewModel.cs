﻿using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.Services;
using Prism;
using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Realms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class CategoriaPageViewModel : ViewModelBase, INavigationAware, IPageLifecycleAware, IActiveAware
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private NavigationMode ModoNavegacion = NavigationMode.New;

        #region Propiedades
        private ObservableCollection<MenuClasificacion> _categorias;

        public ObservableCollection<MenuClasificacion> Categorias
        {
            get => _categorias;
            set => SetProperty(ref _categorias, value);
        }

        private ObservableCollection<Producto> _productos;

        public ObservableCollection<Producto> Productos
        {
            get => _productos;
            set => SetProperty(ref _productos, value);
        }

        private ImageSource _fotoPrincipal;
        public ImageSource FotoPrincipal
        {
            get => _fotoPrincipal;
            set => SetProperty(ref _fotoPrincipal, value);
        }

        private string _tituloPrincipal;
        public string TituloPrincipal
        {
            get => _tituloPrincipal;
            set => SetProperty(ref _tituloPrincipal, value);
        }

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        private bool _isRunningProductos;
        public bool IsRunningProductos
        {
            get => _isRunningProductos;
            set => SetProperty(ref _isRunningProductos, value);
        }

        private bool _showContador;
        public bool ShowContador
        {
            get => _showContador;
            set => SetProperty(ref _showContador, value);
        }

        private int _totalOrden;
        public int TotalOrden
        {
            get => _totalOrden;
            set => SetProperty(ref _totalOrden, value);
        }

        private MenuClasificacion _selectedItem;
        public MenuClasificacion SelectedItem
        {
            get => _selectedItem;
            set => SetProperty(ref _selectedItem, value);
        }

        private Producto _selectedItemSubMenu;
        public Producto SelectedItemSubMenu
        {
            get => _selectedItemSubMenu;
            set => SetProperty(ref _selectedItemSubMenu, value);
        }

        #endregion

        #region Commands
        private DelegateCommand _selectCategoriaCommand;
        public DelegateCommand SelectCategoriaCommand => _selectCategoriaCommand ?? (_selectCategoriaCommand = new DelegateCommand(SetCategoria));

        private DelegateCommand _selectSubMenuCommand;
        public DelegateCommand SelectSubMenuCommand => _selectSubMenuCommand ?? (_selectSubMenuCommand = new DelegateCommand(SetSubMenu));

        private DelegateCommand _openCarCommand;
        public DelegateCommand OpenCarCommand => _openCarCommand ?? (_openCarCommand = new DelegateCommand(OpenCarPage));
        #endregion

        public CategoriaPageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
        }

        #region Event Selected Tab
        public event EventHandler IsActiveChanged;
        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { SetProperty(ref _isActive, value, RaiseIsActiveChanged); }
        }

        protected virtual void RaiseIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        #region Overloads functions
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("showpreprando"))
            {
                var showPreparando = parameters.GetValue<bool>("showpreprando");
                if(showPreparando)
                    OpenPreparandoPage();
            }
                ModoNavegacion = parameters.GetNavigationMode();
        }

        public void OnAppearing()
        {
            MainTabbedPageViewModel.GetInstance().Title = "Categorias";
            MainTabbedPageViewModel.GetInstance().SetBarNavigation(false);
            var realm = Realm.GetInstance();
            TotalOrden = realm.All<Venta>().Count();
            ShowContador = TotalOrden > 0 ? true : false;
            if (ModoNavegacion != NavigationMode.Back)
            {
                LoadCategorias();
                ModoNavegacion = NavigationMode.New;
            }
        }

        public void OnDisappearing()
        {
            Debug.WriteLine("Categoria is disappearing");
        }
        #endregion

        public async void LoadCategorias()
        {
            try
            {
                IsRunning = true;
                IsRunningProductos = true;
                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetCategoriasMenu(url, _app, "/MenuClasificacion");
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }

                Categorias = new ObservableCollection<MenuClasificacion>(
                    response.Result.Select(s => new MenuClasificacion
                    {
                        CodCla = s.CodCla,
                        NomCla = s.NomCla,
                        UrlFoto = s.UrlFoto,
                        Activo = s.Activo,
                        Opacidad = 0.5
                    }).ToList()
                    );

                Categorias.First().Opacidad = 1;
                IsRunning = false;

                SetSeleccion(Categorias.First());
                SetListaProductos(Categorias.First());
            }
            catch (Exception e)
            {
                IsRunning = false;
                var msg = e.Message;
            }
        }

        public void SetSeleccion(MenuClasificacion item)
        {
            TituloPrincipal = item.NomCla;
            FotoPrincipal = new UriImageSource { Uri = new Uri(item.UrlFoto), CachingEnabled = true, CacheValidity = new TimeSpan(1, 0, 0, 0) };
        }

        public async void SetListaProductos(MenuClasificacion item)
        {
            IsRunningProductos = true;

            try
            {
                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetProductosByCategoria(url, _app, "/Menu", item.CodCla);
                if (!response.IsSuccess)
                {
                    IsRunningProductos = false;
                    //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }

                Productos = new ObservableCollection<Producto>(response.Result.ToList());
                IsRunningProductos = false;
            }
            catch (Exception e)
            {
                IsRunningProductos = false;
                var msg = e.Message;
            }
        }

        private void SetCategoria()
        {
            SetSeleccion(SelectedItem);
            SetListaProductos(SelectedItem);

            //Seteamos la opacidad
            Categorias.ToList().ForEach(c =>
            {
                c.Opacidad = c.CodCla == SelectedItem.CodCla ? 1 : 0.5;
            });
        }

        private async void SetSubMenu()
        {
            if (SelectedItemSubMenu != null)
            {
                var parametrosEnviar = new NavigationParameters();
                parametrosEnviar.Add("detalle", SelectedItemSubMenu);

                await _navigationService.NavigateAsync("DetalleSubMenuPage", parametrosEnviar, useModalNavigation: true);
                SelectedItemSubMenu = null;
            }
        }

        public async void OpenCarPage()
        {
            var realm = await Realm.GetInstanceAsync();
            var ordenes = realm.All<Venta>().ToList();
            if (ordenes.Count > 0)
                await _navigationService.NavigateAsync("CarPage", useModalNavigation: true);
            else
                await _navigationService.NavigateAsync("EmptyCarPage", useModalNavigation: true);
        }

        public async void OpenPreparandoPage()
        {
            await _navigationService.NavigateAsync("PreparandoPage", useModalNavigation: true);
        }
    }
}
