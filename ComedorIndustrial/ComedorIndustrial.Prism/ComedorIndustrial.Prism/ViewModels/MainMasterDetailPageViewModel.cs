﻿using ComedorIndustrial.Prism.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Services.Dialogs;
using ComedorIndustrial.Prism.Helper;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class MainMasterDetailPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _editProfileCommand;
        private DelegateCommand _cerrarSesionCommand;
        private readonly IDialogService _dialogService;
        private static MainMasterDetailPageViewModel _instance;

        private ImageSource _foto;
        private string _nombre;

        public MainMasterDetailPageViewModel(INavigationService navigationService,IDialogService dialogService) : base(navigationService)
        {
            _instance = this;
            _navigationService = navigationService;
            _dialogService = dialogService;
            LoadMenus();
            SetProfile();
        }

        public ImageSource Foto
        {
            get => _foto;
            set => SetProperty(ref _foto, value);
        }

        public string Nombre
        {
            get => _nombre;
            set => SetProperty(ref _nombre, value);
        }

        public static MainMasterDetailPageViewModel GetInstance()
        {
            return _instance;
        }

        public ObservableCollection<MenuItemViewModel> Menus { get; set; }

        public DelegateCommand EditProfileCommand => _editProfileCommand ?? (_editProfileCommand = new DelegateCommand(ExecuteEditProfileCommand));
        public DelegateCommand CerrarSesionCommand => _cerrarSesionCommand ?? (_cerrarSesionCommand = new DelegateCommand(ExecuteCerrarSesion));

        private void LoadMenus()
        {
            var menus = new List<ComedorIndustrial.Prism.Model.Menu>
            {
                new ComedorIndustrial.Prism.Model.Menu
                {
                    Icon = "\uf054",
                    PageName = "HomePage",
                    Title = "Inicio",
                    Navegacion = ""
                },
                new ComedorIndustrial.Prism.Model.Menu
                {
                    Icon = "\uf054",
                    PageName = "EncuestaPage",
                    Title = "Encuesta",
                    Navegacion = ""
                },
                new ComedorIndustrial.Prism.Model.Menu
                {
                    Icon = "\uf054",
                    PageName = "NoticiasPage",
                    Title = "Noticias",
                    Navegacion = "/MainMasterDetailPage/NavigationPage/NoticiasPage"
                },
                new ComedorIndustrial.Prism.Model.Menu
                {
                    Icon = "\uf054",
                    PageName = "HomePage",
                    Title = "Políticas de Privacidad",
                    Navegacion = ""
                },
                new ComedorIndustrial.Prism.Model.Menu
                {
                    Icon = "\uf054",
                    PageName = "HomePage",
                    Title = "Ocasiones Especiales"
                },
                new ComedorIndustrial.Prism.Model.Menu
                {
                    Icon = "\uf054",
                    PageName = "HomePage",
                    Title = "Sobre Nosotros",
                    Navegacion = ""
                }
            };

            Menus = new ObservableCollection<MenuItemViewModel>(
                menus.Select(m => new MenuItemViewModel(_navigationService)
                {
                    Icon = m.Icon,
                    PageName = m.PageName,
                    Title = m.Title
                }).ToList());
        }

        async void ExecuteEditProfileCommand()
        {            
            await _navigationService.NavigateAsync("ProfilePage", useModalNavigation: true);
        }

        async void ExecuteCerrarSesion()
        {
            Settings.IsLogin = false;
            Settings.User = string.Empty;
            await _navigationService.NavigateAsync("/LoginPage");
        }

        public void SetProfile()
        {
            var user = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            Foto = new UriImageSource
            {
                Uri = new Uri(user.Foto),
                CachingEnabled = false,
                CacheValidity = new TimeSpan(1, 0, 0, 0)
            };
            Nombre = user.Nombre;
        }
    }
}
