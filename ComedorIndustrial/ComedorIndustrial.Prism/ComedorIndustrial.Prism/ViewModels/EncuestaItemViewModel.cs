﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class EncuestaItemViewModel
    {
        public int IdEncuesta { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string CodCentroCosto { get; set; }
        public List<DetallaEncViewModel> Detalle { get; set; }



        public void EnumeracionPreguntas()
        {
            int conta = 0;
            foreach (var item in Detalle)
            {
                conta++;
                item.PiePregunta = conta + " de " + Detalle.Count;

            }
        }
    }
}
