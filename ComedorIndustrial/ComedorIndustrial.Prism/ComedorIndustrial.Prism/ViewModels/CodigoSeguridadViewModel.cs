﻿using ComedorIndustrial.Prism.Helper;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class CodigoSeguridadViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;
        private DelegateCommand _navigateCodigoSegCommand;

        private string _codigo;
        private string _errorTextCodigo;
        private bool _hasErrorCodigo;

        private string codeChar0;
        private string codeChar1;
        private string codeChar2;
        private string codeChar3;
        private string codeChar4;

        public string CodigoWebServiceCompare { get; set; }
        public string Usuario { get; set; }

        public string Codigo
        {
            get => _codigo;
            set => SetProperty(ref _codigo, value);
        }
        public string ErrorTextCodigo
        {
            get => _errorTextCodigo;
            set => SetProperty(ref _errorTextCodigo, value);
        }

        public bool HasErrorCodigo
        {
            get => _hasErrorCodigo;
            set => SetProperty(ref _hasErrorCodigo, value);
        }

        public string CodeChar0
        {
            get => codeChar0;
            set => SetProperty(ref codeChar0, value);
        }

        public string CodeChar1
        {
            get => codeChar1;
            set => SetProperty(ref codeChar1, value);
        }
        public string CodeChar2
        {
            get => codeChar2;
            set => SetProperty(ref codeChar2, value);
        }
        public string CodeChar3
        {
            get => codeChar3;
            set => SetProperty(ref codeChar3, value);
        }
        public string CodeChar4
        {
            get => codeChar4;
            set => SetProperty(ref codeChar4, value);
        }


        public DelegateCommand NavigateCodigoSegCommand => _navigateCodigoSegCommand ?? (_navigateCodigoSegCommand = new DelegateCommand(ExecuteNavCodSegCommand));

        public CodigoSeguridadViewModel(INavigationService navigationService, IDialogService dialogService) : base(navigationService)
        {
 
            _navigationService = navigationService;
            _dialogService = dialogService;
        }
 


        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            CodigoWebServiceCompare = parameters.GetValue<string>("CodigoWs");
            Usuario = parameters.GetValue<string>("Usuario");
        }


        async void ExecuteNavCodSegCommand()
        {
            Codigo = CodeChar0 + CodeChar1 + CodeChar2 + CodeChar3 + CodeChar4;

            if(Codigo.Length < 5)
            {
                CustomDialog.ShowAlert(_dialogService, "Error", "Ingrese el codigo completo");                
                return;
            }


            if (Codigo != CodigoWebServiceCompare)
            {
                CustomDialog.ShowAlert(_dialogService, "Error", "Código Incorrecto");                
                return;
            }

            
            var parametrosEnviar = new NavigationParameters();
            parametrosEnviar.Add("Usuario", Usuario);
            
            await _navigationService.NavigateAsync("CambiarContrasena", parametrosEnviar);
          
        }


    }
}
