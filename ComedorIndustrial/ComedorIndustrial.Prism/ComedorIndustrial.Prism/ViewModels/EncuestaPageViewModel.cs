﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class EncuestaPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private static EncuestaPageViewModel _instance;

        private bool _isRunning;
        private bool _isVisible;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        private bool _introEncuestaVisible;
        public bool IntroEncuestaVisible
        {
            get => _introEncuestaVisible;
            set => SetProperty(ref _introEncuestaVisible, value);
        }

        private ObservableCollection<DetallaEncViewModel> _preguntas;
        public ObservableCollection<DetallaEncViewModel> Preguntas
        {
            get => _preguntas;
            set => SetProperty(ref _preguntas, value);
        }

        private EncuestaItemViewModel _encuestas;
        public EncuestaItemViewModel Encuestas
        {
            get { return _encuestas; }
            set => SetProperty(ref _encuestas, value);
        }

        private DelegateCommand _verencuesta;
        public DelegateCommand EncuCommand => _verencuesta ?? (_verencuesta = new DelegateCommand(ExecuteVerEncuesta));

        public EncuestaPageViewModel(INavigationService navigationService, IApiService apiService):base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _instance = this;
            IntroEncuestaVisible = true;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.New)
            {
                IntroEncuestaVisible = true;
                LoadData();
                //if (Device.RuntimePlatform == Device.iOS)
                //{
                //    VisibleCarousel = true;
                //    IntroEncuestaVisible = false;
                //}
            }
        }

        public static EncuestaPageViewModel GetInstance()
        {
            return _instance;
        }

        private async void LoadData()
        {

            IsRunning = true;
            IsVisible = false;
            try
            {

                var cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
                var Controller = "/Encuestas?CodCentroCosto=&CodCliente=" + cliente.CodCliente;

                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetEncuesta(url, _app, Controller);
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    //VisibleNoEncuesta = true;
                    //VisibleCarousel = false;
                    //IntroEncuestaVisible = false;
                    return;
                }


                //VisibleNoEncuesta = false;
                //VisibleCarousel = false;
                //IntroEncuestaVisible = true;

                var _dataLista = response.Result;
                var _data = _dataLista[0];

                Encuestas = new EncuestaItemViewModel()
                {
                    IdEncuesta = _data.IdEncuesta,
                    Titulo = _data.Titulo,
                    Descripcion = _data.Descripcion,
                    CodCentroCosto = _data.CodCentroCosto,
                    Detalle = _data.Detalle.Select(x => new DetallaEncViewModel()
                    {
                        Descripcion = x.Descripcion,
                        IdPregunta = x.IdPregunta,
                        Opiniones = x.Opiniones.Select(y => new OpinionViewModel()
                        {
                            IdRespuesta = y.IdRespuesta,
                            IdTipoOpinion = y.IdTipoOpinion,
                            TextoAbierto = y.TextoAbierto,
                            Titulo = y.Titulo
                        }).ToList()
                    }).ToList()
                };

                Encuestas.EnumeracionPreguntas();

                Preguntas = new ObservableCollection<DetallaEncViewModel>(
                    Encuestas.Detalle.Select(x => new DetallaEncViewModel()
                    {
                        Descripcion = x.Descripcion,
                        IdPregunta = x.IdPregunta,
                        Opiniones = x.Opiniones.Select(y => new OpinionViewModel()
                        {
                            IdRespuesta = y.IdRespuesta,
                            IdTipoOpinion = y.IdTipoOpinion,
                            TextoAbierto = y.TextoAbierto,
                            Titulo = y.Titulo
                        }).ToList(),
                        PiePregunta = x.PiePregunta,
                        OpcionSeleccionda = x.OpcionSeleccionda,
                        TextoRespuesta = x.TextoRespuesta
                    }).ToList());


                await Task.Delay(1000);
                IsRunning = false;
                IsVisible = true;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }

        public async void EnviarEncuestas()
        {
            var codigoCliente = (JsonConvert.DeserializeObject<Cliente>(Settings.User)).CodCliente;
            var request = Encuestas.Detalle.Select(x => new EncuestaRequest()
            {
                CodCliente = codigoCliente,
                IdEncuesta = Encuestas.IdEncuesta,
                IdOpinion = (x.OpcionSeleccionda == null) ? x.Opiniones.FirstOrDefault().IdTipoOpinion : x.OpcionSeleccionda.IdTipoOpinion,
                IdPregunta = x.IdPregunta,
                IdRespuesta = (x.OpcionSeleccionda == null) ? x.Opiniones.FirstOrDefault().IdRespuesta : x.OpcionSeleccionda.IdRespuesta,
                TextoRespuesta = (x.TextoRespuesta == null) ? "" : x.TextoRespuesta
            }).ToList();


            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.SendResultEncuesta(url, _app, "/EncuestasRegistro", request);

            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }

            await Task.Delay(1000);
        }

        public async void ExecuteVerEncuesta()
        {
            IntroEncuestaVisible = false;
        }
    }
}
