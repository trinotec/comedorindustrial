﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class MainTabbedPageViewModel : ViewModelBase
    {
        private static MainTabbedPageViewModel _instance;
        public MainTabbedPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            _instance = this;
            Title = "Menú";
        }

        public static MainTabbedPageViewModel GetInstance()
        {
            return _instance;
        }

        public void SetBarNavigation(bool visible)
        {
            OnNavigationBarSet(visible);
        }

        #region delegate method
        internal delegate void NavigationBarSetting(bool visible);

        internal NavigationBarSetting navigationBarSet;

        void OnNavigationBarSet(bool visible)
        {
            navigationBarSet(visible);
        }
        #endregion


    }
}
