﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Realms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms.Internals;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class CarPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private NavigationMode ModoNavegacion;
        private List<Venta> Ventas;

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        private bool _isRunningCar;
        public bool IsRunningCar
        {
            get => _isRunningCar;
            set => SetProperty(ref _isRunningCar, value);
        }

        private string _nombre;
        public string Nombre
        {
            get => _nombre;
            set => SetProperty(ref _nombre, value);
        }

        private string _codCliente;
        public string CodCliente
        {
            get => _codCliente;
            set => SetProperty(ref _codCliente, value);
        }

        private float _subTotalOrden;
        public float SubTotalOrden
        {
            get => _subTotalOrden;
            set => SetProperty(ref _subTotalOrden, value);
        }

        private float _totalOrden;
        public float TotalOrden
        {
            get => _totalOrden;
            set => SetProperty(ref _totalOrden, value);
        }

        private float _envio;
        public float Envio
        {
            get => _envio;
            set => SetProperty(ref _envio, value);
        }

        private ObservableCollection<DesgloseVenta> _productos;

        public ObservableCollection<DesgloseVenta> Productos
        {
            get => _productos;
            set => SetProperty(ref _productos, value);
        }

        private DelegateCommand _sendVentaCommand;
        public DelegateCommand SendVentaCommand => _sendVentaCommand ?? (_sendVentaCommand = new DelegateCommand(EnviarVenta));

        public CarPageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            var cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            Nombre = cliente.Nombre;
            CodCliente = cliente.CodCliente;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            ModoNavegacion = parameters.GetNavigationMode();
            var realm = Realm.GetInstance();
            Ventas = realm.All<Venta>().ToList();
            LoadProductosCarrito();

        }

        public async void LoadProductosCarrito()
        {
            try
            {
                IsRunningCar = true;
                Productos = new ObservableCollection<DesgloseVenta>();
                SubTotalOrden = Ventas.Sum(s => s.ImporteVenta);
                Envio = 500;
                TotalOrden = SubTotalOrden + Envio;
                Ventas.ForEach(v =>
                {
                    //Obtenemos el producto principal
                    var principal = v.DetalleVentas.Where(w => w.EsPrincipal).FirstOrDefault();
                    var secundarios = v.DetalleVentas.Where(w => w.EsPrincipal == false && w.Marcas > 0).Select(s => s.Leyenda).ToList();
                    string _descripcion = secundarios.Count() > 0 ? secundarios.Aggregate((i, j) => i + ", " + j) : principal.DescripcionLarga;
                    string _titulop = principal.TotalOrdenados > 1 ?  $"{principal.TotalOrdenados} {principal.Leyenda}" : $"{principal.Leyenda}";
                    var p1 = new DesgloseVenta
                    {
                        Titulo = _titulop,
                        Precio = principal.PreVta,
                        Descripcion = _descripcion
                    };
                    Productos.Add(p1);

                    var productosAdicionales = v.DetalleVentas.Where(w => w.EsPrincipal == false && w.Marcas == 0).ToList();
                    productosAdicionales.ForEach(p =>
                    {
                        string _titulo = $"{p.TotalOrdenados} {p.Leyenda}";
                        Productos.Add(new DesgloseVenta
                        {
                            Descripcion = p.Leyenda,
                            Precio = p.PreVta,
                            Titulo = _titulo
                        });
                    });
                });
                IsRunningCar = false;
            }
            catch(Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error", e.Message.ToString(), "Ok");
            }

        }

        public async void EnviarVenta()
        {
            IsRunning = true;
            var encVtas = new EncVtas
            {
                TipoFactura = "WE",
                CodCliente = "60336243",
                NomCliente = Nombre,
                CodRest = "01",
                Subtotal = SubTotalOrden,
                Descuento = 0,
                ImptoVentas = 0,
                FleteExpress = Envio,
                NumTar = "721652",
                MtoTar = TotalOrden,
                NumReferencia = "",
                NumAutorizacion = "",
                NumTarjetaCF = ""
            };

            var detVentas = new List<DetVtas>();
            int numlinea = 0;
            Ventas.ForEach(v=> {
                v.DetalleVentas.ForEach(p=> {
                    numlinea++;
                    detVentas.Add(new DetVtas
                    {
                        CodPro = p.Codigo,
                        NomPro = p.Leyenda,
                        PorDescto = 0,
                        uniVen = p.TotalOrdenados,
                        PreVenta = p.PreVta,
                        Principal = p.EsPrincipal,
                        NumLinea = numlinea,
                        Comentarios = p.Comentarios
                    });
                });
            });

            var request = new VentaRequest
            {
                encVtas = encVtas,
                detVtas = detVentas
            };


            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.SendVenta(url, _app, "/Ventas", request);

            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }

            var realm = await Realm.GetInstanceAsync();
            using (var trans = realm.BeginWrite())
            {
                realm.RemoveAll<Venta>();
                trans.Commit();
            }
            IsRunning = false;

            var parametrosEnviar = new NavigationParameters();
            parametrosEnviar.Add("showpreprando", true);
            await _navigationService.GoBackAsync(parametrosEnviar, useModalNavigation: true);            
        }
    }
}
