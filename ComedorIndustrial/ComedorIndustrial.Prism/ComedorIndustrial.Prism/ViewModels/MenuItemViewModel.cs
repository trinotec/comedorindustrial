﻿using ComedorIndustrial.Prism.Model;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class MenuItemViewModel : Menu
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _selectMenuCommand;

        public MenuItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public DelegateCommand SelectMenuCommand => _selectMenuCommand ?? (_selectMenuCommand = new DelegateCommand(SelectMenu));

        private async void SelectMenu()
        {
            if (PageName.Equals("HomePage"))
            {
                var data = _navigationService.GetNavigationUriPath();
                await _navigationService.NavigateAsync(new Uri("NavigationPage/MainTabbedPage", UriKind.Relative));
            }
            else
            {
                var data = _navigationService.GetNavigationUriPath();
                await _navigationService.NavigateAsync(new Uri($"NavigationPage/{PageName}", UriKind.Relative));
            }

           
        }
    }
}
