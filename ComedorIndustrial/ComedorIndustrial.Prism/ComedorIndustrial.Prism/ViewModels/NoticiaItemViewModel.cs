﻿using ComedorIndustrial.Prism.Model;
using Prism.Commands;
using Prism.Navigation;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class NoticiaItemViewModel : Noticia
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _selectNoticiaCommand;

        public NoticiaItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public DelegateCommand SelectNoticiaCommand => _selectNoticiaCommand ?? (_selectNoticiaCommand = new DelegateCommand(ShowNoticia));

        private async void ShowNoticia()
        {
            var parameters = new NavigationParameters();
            parameters.Add("noticia", this);

            await _navigationService.NavigateAsync("NoticiaDetallePage", parameters);
        }
    }
}
