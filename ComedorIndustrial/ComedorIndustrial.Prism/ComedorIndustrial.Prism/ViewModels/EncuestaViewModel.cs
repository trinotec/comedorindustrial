﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Services;
using ComedorIndustrial.Prism.Model;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Services.Dialogs;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class EncuestaViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private static EncuestaViewModel _instance;

        private EncuestaItemViewModel _encuestas;

        private bool _isRunning;
        private bool _isVisible;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        private bool _visibleNoEncuesta;
        public bool VisibleNoEncuesta
        {
            get => _visibleNoEncuesta;
            set => SetProperty(ref _visibleNoEncuesta, value);
        }


        private bool _visibleCarousel;
        public bool VisibleCarousel
        {
            get => _visibleCarousel;
            set => SetProperty(ref _visibleCarousel, value);
        }

        private bool _introEncuestaVisible;
        public bool IntroEncuestaVisible
        {
            get => _introEncuestaVisible;
            set => SetProperty(ref _introEncuestaVisible, value);
        }

        public EncuestaItemViewModel Encuestas
        {
            get { return _encuestas; }
            set => SetProperty(ref _encuestas, value);
        }

        //private ObservableCollection<DetallaEncViewModel> _preguntas;
        //public ObservableCollection<DetallaEncViewModel> Preguntas
        //{
        //    get => _preguntas;
        //    set => SetProperty(ref _preguntas, value);
        //}


        private DelegateCommand _verencuesta;
        public DelegateCommand EncuCommand => _verencuesta ?? (_verencuesta = new DelegateCommand(ExecuteVerEncuesta));

        public async void ExecuteVerEncuesta()
        {
            VisibleCarousel = true;
            IntroEncuestaVisible = false;
            //await _navigationService.NavigateAsync("/MainMasterDetailPage/NavigationPage/MainTabbedPage");
        }

        public EncuestaViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            _instance = this;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.New)
            {
                LoadData();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    VisibleCarousel = true;
                    IntroEncuestaVisible = false;
                }
            }
        }

        public static EncuestaViewModel GetInstance()
        {
            return _instance;
        }

        private async void LoadData()
        {

            IsRunning = true;
            IsVisible = false;
            try
            {

                var Cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
                var Controller = "/Encuestas?CodCentroCosto=&CodCliente=" + Cliente.CodCliente;

                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetEncuesta(url, _app, Controller);
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    VisibleNoEncuesta = true;
                    VisibleCarousel = false;
                    IntroEncuestaVisible = false;
                    return;
                }


                VisibleNoEncuesta = false;
                VisibleCarousel = false;
                IntroEncuestaVisible = true;

                var _dataLista = response.Result;
                var _data = _dataLista[0];

                Encuestas = new EncuestaItemViewModel()
                {
                    IdEncuesta = _data.IdEncuesta,
                    Titulo = _data.Titulo,
                    Descripcion = _data.Descripcion,
                    CodCentroCosto = _data.CodCentroCosto,
                    Detalle = _data.Detalle.Select(x => new DetallaEncViewModel()
                    {
                        Descripcion = x.Descripcion,
                        IdPregunta = x.IdPregunta,
                        Opiniones = x.Opiniones.Select(y => new OpinionViewModel()
                        {
                            IdRespuesta = y.IdRespuesta,
                            IdTipoOpinion = y.IdTipoOpinion,
                            TextoAbierto = y.TextoAbierto,
                            Titulo = y.Titulo
                        }).ToList()
                    }).ToList()
                };

                Encuestas.EnumeracionPreguntas();

                //Preguntas = new ObservableCollection<DetallaEncViewModel>(
                //    Encuestas.Detalle.Select(x => new DetallaEncViewModel()
                //    {
                //        Descripcion = x.Descripcion,
                //        IdPregunta = x.IdPregunta,
                //        Opiniones = x.Opiniones.Select(y => new OpinionViewModel()
                //        {
                //            IdRespuesta = y.IdRespuesta,
                //            IdTipoOpinion = y.IdTipoOpinion,
                //            TextoAbierto = y.TextoAbierto,
                //            Titulo = y.Titulo
                //        }).ToList(),
                //        PiePregunta = x.PiePregunta,
                //        OpcionSeleccionda = x.OpcionSeleccionda,
                //        TextoRespuesta = x.TextoRespuesta
                //    }).ToList());


                await Task.Delay(1000);
                IsRunning = false;
                IsVisible = true;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }

        public async void EnviarEncuestas()
        {
            var codigoCliente = (JsonConvert.DeserializeObject<Cliente>(Settings.User)).CodCliente;
            var request = Encuestas.Detalle.Select(x => new EncuestaRequest()
            {
                CodCliente = codigoCliente,
                IdEncuesta = Encuestas.IdEncuesta,
                IdOpinion = (x.OpcionSeleccionda == null) ? x.Opiniones.FirstOrDefault().IdTipoOpinion : x.OpcionSeleccionda.IdTipoOpinion,
                IdPregunta = x.IdPregunta,
                IdRespuesta = (x.OpcionSeleccionda == null) ? x.Opiniones.FirstOrDefault().IdRespuesta : x.OpcionSeleccionda.IdRespuesta,
                TextoRespuesta = (x.TextoRespuesta == null) ? "" : x.TextoRespuesta
            }).ToList();


            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.SendResultEncuesta(url, _app, "/EncuestasRegistro", request);

            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }

            await Task.Delay(1000);
        }

    }
}
