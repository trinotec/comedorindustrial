﻿using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Realms;
using Syncfusion.DataSource.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class DetalleSubMenuPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private static DetalleSubMenuPageViewModel _instance;

        #region Propiedades
        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        private bool _showPajilla;
        public bool ShowPajilla
        {
            get => _showPajilla;
            set => SetProperty(ref _showPajilla, value);
        }

        private Producto _producto;
        public Producto Producto
        {
            get => _producto;
            set => SetProperty(ref _producto, value);
        }

        private float _totalOrden;
        public float TotalOrden
        {
            get => _totalOrden;
            set => SetProperty(ref _totalOrden, value);
        }

        private int _cantidadOrden;
        public int CantidadOrden
        {
            get => _cantidadOrden;
            set
            {
                CheckIncrementCantidad(value);
                SetProperty(ref _cantidadOrden, value);
            }
        }

        private bool _requierePajilla;
        public bool RequierePajilla
        {
            get => _requierePajilla;
            set => SetProperty(ref _requierePajilla, value);
        }

        private bool _showContador;
        public bool ShowContador
        {
            get => _showContador;
            set => SetProperty(ref _showContador, value);
        }

        private int _totalPedido;
        public int TotalPedido
        {
            get => _totalPedido;
            set => SetProperty(ref _totalPedido, value);
        }

        private bool _isComplete;
        public bool IsComplete
        {
            get => _isComplete;
            set => SetProperty(ref _isComplete, value);
        }

        private string _comentario;
        public string Comentario
        {
            get => _comentario;
            set => SetProperty(ref _comentario, value);
        }

        private ObservableCollection<DetalleSubMenu> _detalleSubMenu;

        public ObservableCollection<DetalleSubMenu> DetalleSubMenu
        {
            get => _detalleSubMenu;
            set => SetProperty(ref _detalleSubMenu, value);
        }

        private ObservableCollection<GrupoSubMenu> _grupoSubMenu;

        public ObservableCollection<GrupoSubMenu> GrupoSubMenu
        {
            get => _grupoSubMenu;
            set => SetProperty(ref _grupoSubMenu, value);
        }

        #endregion

        private DelegateCommand _openCarCommand;
        public DelegateCommand OpenCarCommand => _openCarCommand ?? (_openCarCommand = new DelegateCommand(OpenCarPage));

        private DelegateCommand _incrementTotalCommand;
        public DelegateCommand IncrementCantidadCommand => _incrementTotalCommand ?? (_incrementTotalCommand = new DelegateCommand(IncrementCantidad));

        private DelegateCommand _decrementTotalCommand;
        public DelegateCommand DecrementCantidadCommand => _decrementTotalCommand ?? (_decrementTotalCommand = new DelegateCommand(DecrementCantidad));

        private DelegateCommand _addOrdenCommand;
        public DelegateCommand AddOrdenCommand => _addOrdenCommand ?? (_addOrdenCommand = new DelegateCommand(AddOrden));

        private void IncrementCantidad()
        {
            CantidadOrden++;
        }

        private void DecrementCantidad()
        {
            CantidadOrden--;
        }

        public DetalleSubMenuPageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _instance = this;
            _navigationService = navigationService;
            _apiService = apiService;
        }

        public static DetalleSubMenuPageViewModel GetInstance()
        {
            return _instance;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("detalle"))
            {
                Producto = parameters.GetValue<Producto>("detalle");
                TotalOrden = Producto.PreVta;
                CantidadOrden = 1;
                var realm = Realm.GetInstance();
                TotalPedido = realm.All<Venta>().Count();
                ShowContador = TotalPedido > 0 ? true : false;
                LoadDetalle();
            }

            if (parameters.ContainsKey("showpreprando"))
            {
                var showPreparando = parameters.GetValue<bool>("showpreprando");
                if (showPreparando)
                    OpenPreparandoPage();
            }
        }

        public async void LoadDetalle()
        {
            try
            {
                IsRunning = true;
                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetDetalleSubMenu(url, _app, "/SubMenu", Producto.NumMenu); //Producto.NumMenu
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }
                GenerateGrupos(response.Result.ToList());

                DetalleSubMenu = new ObservableCollection<DetalleSubMenu>(
                    list: response.Result.ToList()
                    );

                ShowPajilla = DetalleSubMenu.Count() > 0 ? true : false;
                CheckComplete();

                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                var msg = e.Message;
            }
        }

        public async void OpenCarPage()
        {
            var realm = await Realm.GetInstanceAsync();
            var ordenes = realm.All<Venta>().ToList();
            if( ordenes.Count > 0)
                await _navigationService.NavigateAsync("CarPage", useModalNavigation: true);
            else
                await _navigationService.NavigateAsync("EmptyCarPage", useModalNavigation: true);
        }

        private void GenerateGrupos(List<DetalleSubMenu> data)
        {
            List<DetalleSubMenu> detalles = new List<DetalleSubMenu>();
            int grupo = 0;

            data.ForEach(d=> { 
                if(d.TipoMenu == "M") { grupo++; }
                d.Grupo = grupo;
                detalles.Add(d);
            });

            var grupos = data.GroupBy(g => new { g.Grupo })
                .Select(s => new GrupoSubMenu { 
                    Id = s.Key.Grupo,
                    Encabezado = s.Where(w => w.TipoMenu == "M").FirstOrDefault(), 
                    Opciones = s.Where(w => w.TipoMenu != "M").ToList() 
                })
                .ToList();

            GrupoSubMenu = new ObservableCollection<GrupoSubMenu>(grupos);

        }

        public void CheckComplete()
        {
            bool _complete = true;
            var gruposSelection = GrupoSubMenu.Where(w => w.Encabezado.Marcas > 0).ToList();
            gruposSelection.ForEach(g=> {
                if (g.Opciones.Where(w => w.Seleccionado == true).Count() < g.Encabezado.Marcas)
                    _complete = false;
            });

            IsComplete = _complete;
        }

        private void CheckIncrementCantidad(int newValue)
        {
            if (CantidadOrden > 0)
            {
                var precioUnidad = TotalOrden / CantidadOrden;
                TotalOrden = precioUnidad * newValue;
            }
        }

        private async void AddOrden()
        {
            try
            {
                var realm = await Realm.GetInstanceAsync();
                var ordenes = realm.All<Venta>().ToList();
                var indice = 0;
                if (ordenes.Count > 0)
                    indice = ordenes.Max(s => s.Id);

                Venta _venta = new Venta
                {
                    Id = indice + 1,
                    CantidadVenta = CantidadOrden,
                    RequirePajilla = RequierePajilla,
                    ImporteVenta = TotalOrden,
                };

                //Se agregan los prodcutos
                _venta.DetalleVentas.Add(new DetalleVenta
                {
                    CodCla = Producto.CodCla,
                    EsPrincipal = true,
                    Codigo = Producto.Codigo,
                    Leyenda = Producto.DescripcionCorta,
                    DescripcionLarga = Producto.DescripcionLarga,
                    NumItem = 0,
                    NumMenu = Producto.NumMenu,
                    TotalOrdenados = CantidadOrden,
                    PreVta = Producto.PreVta,
                    Comentarios = Comentario,
                    TipoMenu = Producto.TipoMenu
                });

                //Agregamos los demas productos
                GrupoSubMenu.ForEach(g=> {
                    g.Opciones.ForEach(p=> { 
                        if(g.Encabezado.Marcas > 0)
                        {
                            if (p.Seleccionado)
                            {
                                var _det = new DetalleVenta
                                {
                                    CodCla = Producto.CodCla,
                                    EsPrincipal = false,
                                    Codigo = p.Codigo,
                                    Leyenda = p.Leyenda,
                                    NumItem = p.NumItem,
                                    NumMenu = p.NumMenu,
                                    Marcas = 1,
                                    TotalOrdenados = CantidadOrden,
                                    PreVta = (float)p.PreVta,
                                    TipoMenu = p.TipoMenu
                                };
                                _venta.DetalleVentas.Add(_det);
                            }
                        }
                        else
                        {
                            if(p.Total > 0)
                            {
                                var _det = new DetalleVenta
                                {
                                    CodCla = Producto.CodCla,
                                    EsPrincipal = false,
                                    Codigo = p.Codigo,
                                    Leyenda = p.Leyenda,
                                    NumItem = p.NumItem,
                                    NumMenu = p.NumMenu,
                                    Marcas = 0,
                                    TotalOrdenados = p.Total * CantidadOrden,
                                    PreVta = (float)p.PreVta,
                                    TipoMenu = p.TipoMenu
                                };
                                _venta.DetalleVentas.Add(_det);
                            }
                        }
                    });
                });

                await realm.WriteAsync(temp=> {
                    temp.Add(_venta);
                });

                await _navigationService.GoBackAsync();

            }
            catch (Exception ex)
            {
                // Handle exception that occurred while opening the Realm
            }
        }

        public async void OpenPreparandoPage()
        {
            await _navigationService.NavigateAsync("PreparandoPage", useModalNavigation: true);
        }
    }
}
