﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Services;
using ComedorIndustrial.Prism.Model;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Prism.AppModel;
using System.Diagnostics;
using Prism;

namespace ComedorIndustrial.Prism.ViewModels
{

    public class FacturasGralPageViewModel : ViewModelBase, INavigationAware, IPageLifecycleAware, IActiveAware
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        public event EventHandler IsActiveChanged;
        private DelegateCommand _navigateFechas;
        private DateTime _fechaInicio;
        private DateTime _fechaFin;
        private string _txtFechasRango;
        private string _nombreUsuario;


        private static FacturasGralPageViewModel _instance;

        private bool _isRunning;

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public string NombreUsuario
        {
            get => _nombreUsuario;
            set => SetProperty(ref _nombreUsuario, value);
        }

        public string TxtFechasRango
        {
            get => _txtFechasRango;
            set => SetProperty(ref _txtFechasRango, value);
        }


        private TransaccionConsumoDetallado _transacciones;
        public TransaccionConsumoDetallado Transacciones
        {
            get => _transacciones;
            set => SetProperty(ref _transacciones, value);
        }

        private ObservableCollection<MovimientoDetallado> _movimientos;
        public ObservableCollection<MovimientoDetallado> Movimientos
        {
            get => _movimientos;
            set => SetProperty(ref _movimientos, value);
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { SetProperty(ref _isActive, value, RaiseIsActiveChanged); }
        }

        protected virtual void RaiseIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);
        }

        public DelegateCommand CommandFechas => _navigateFechas ?? (_navigateFechas = new DelegateCommand(ExecuteModalFechas));
        public FacturasGralPageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            _fechaInicio = DateTime.Now.AddDays(-30);
            _fechaFin = DateTime.Now;
            _instance = this;
            var cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            NombreUsuario = cliente.Nombre;

        }

        public void ExecuteModalFechas()
        {
            _navigationService.NavigateAsync("FechasFacturasPage", useModalNavigation: true);
        }

        public static FacturasGralPageViewModel GetInstance()
        {
            return _instance;
        }

        public void SetTxtFechaRango()
        {
            TxtFechasRango = "Del " + _fechaInicio.ToString("dd/MM/yy") + " al " + _fechaFin.ToString("dd/MM/yy");
        }


        public async void load()
        {
            SetTxtFechaRango();
            IsRunning = true;

            try
            {
                var cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
                var transaccionRequest = new TransaccionRequest
                {
                    CodCliente = cliente.CodCliente,                                 
                    fechaInicial = _fechaInicio.ToString("MM/dd/yyyy"),
                    fechaFinal = _fechaFin.ToString("MM/dd/yyyy")
                };

                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetTransaccionesDetalle(url, _app, "/Transacciones/ConsumoDetallado", transaccionRequest);
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }
                IsRunning = false;                
                Transacciones = response.Result;
                

                if (response.Result.movimientos.Count > 0)
                {
                    Movimientos = new ObservableCollection<MovimientoDetallado>(
                        response.Result.movimientos.Select(
                        l => new MovimientoDetallado()
                        {
                            Credito = l.Credito,
                            Fecha = l.Fecha,
                            MostrarDetalle = l.MostrarDetalle,
                            NumFactura = l.NumFactura,
                            Otros = l.Otros,
                            Subsidio = l.Subsidio,
                            TipoTransaccion = l.TipoTransaccion,
                            TotalFactura = l.TotalFactura,
                            Detalle = l.Detalle,
                            HeightRequest = 25 * l.Detalle.Count + (l.Detalle.Count > 0 ? 50 : 40)
                        })
                        .ToList()
                        );
                }
                else
                {
                    Movimientos = new ObservableCollection<MovimientoDetallado>();
                }
                


            }
            catch (Exception e)
            {
                IsRunning = false;
                var msg = e.Message;
            }

        }

        public void UpdateFacturas(DateTime fechaInicioAct, DateTime fechaFinAct)
        {           
            _fechaInicio = fechaInicioAct;
            _fechaFin = fechaFinAct;
            load();
        }

        public void OnAppearing()
        {
            MainTabbedPageViewModel.GetInstance().Title = "Estado";
            MainTabbedPageViewModel.GetInstance().SetBarNavigation(true);
            load();
        }

        public void OnDisappearing()
        {
            Debug.WriteLine("MyTabbedPage is disappearing");
        }

         
    }
}
