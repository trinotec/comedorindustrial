﻿using ComedorIndustrial.Prism.Helper;
using ComedorIndustrial.Prism.Model;
using ComedorIndustrial.Prism.Services;
using Prism.AppModel;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class HomePageViewModel : ViewModelBase, IPageLifecycleAware
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private bool _isRunning;
        private bool _isPlaying;
        private bool _isVisible;
        private ObservableCollection<PlatilloItemViewModel> _platillosSaludable;
        private ObservableCollection<PlatilloItemViewModel> _platillosSemanal;
        private ObservableCollection<PlatilloItemViewModel> _platillosChef;
        private ObservableCollection<PlatilloItemViewModel> _platillosCombo;
        private ObservableCollection<PlatilloItemViewModel> _platillosReposteria;
        private Publicidad _publicidad;
        

        public HomePageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService) : base(navigationService)
        {
            this._navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            IsRunning = true;
            IsVisible = false;
        }

        public bool IsPlaying
        {
            get => _isPlaying;
            set => SetProperty(ref _isPlaying, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        public ObservableCollection<PlatilloItemViewModel> PlatillosSaludable {
            get => _platillosSaludable;
            set => SetProperty(ref _platillosSaludable, value);
        }

        public ObservableCollection<PlatilloItemViewModel> PlatillosSemanal
        {
            get => _platillosSemanal;
            set => SetProperty(ref _platillosSemanal, value);
        }

        public ObservableCollection<PlatilloItemViewModel> PlatillosChef
        {
            get => _platillosChef;
            set => SetProperty(ref _platillosChef, value);
        }

        public ObservableCollection<PlatilloItemViewModel> PlatillosCombo
        {
            get => _platillosCombo;
            set => SetProperty(ref _platillosCombo, value);
        }

        public ObservableCollection<PlatilloItemViewModel> PlatillosReposteria
        {
            get => _platillosReposteria;
            set => SetProperty(ref _platillosReposteria, value);
        }

        public Publicidad Publicidad
        {
            get => _publicidad;
            set => SetProperty(ref _publicidad, value);
        }

        public void OnAppearing()
        {
            MainTabbedPageViewModel.GetInstance().Title = "Menú";
            MainTabbedPageViewModel.GetInstance().SetBarNavigation(true);
        }

        public void OnDisappearing()
        {
            Debug.WriteLine("MyTabbedPage is disappearing");
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.New)
            {
                LoadData();
            }
        }

        private async void LoadData()
        {
            IsRunning = true;
            IsVisible = false;
            try
            {
                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetMenuByCategoria(url, _app, "/Clientes/RecuperarImagenes");
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }

                var _data = response.Result;
                PlatillosSaludable = new ObservableCollection<PlatilloItemViewModel>(
                    _data.Dia.Select(
                        s => new PlatilloItemViewModel(_navigationService)
                        {
                            CodBanner = s.CodBanner,
                            url = s.url,
                            Activo = s.Activo,
                            ImagenMenu = s.ImagenMenu,
                            Nombre = s.Nombre,
                            Tipo = s.Tipo,
                            Acompanamiento = s.Acompanamiento,
                            Dia = s.Dia,
                            Semana = s.Semana,
                            url2 = s.url2,
                            Calorias = s.Calorias
                        })
                    .ToList());

                PlatillosSemanal = new ObservableCollection<PlatilloItemViewModel>(
                    _data.Semana.Select(
                        s => new PlatilloItemViewModel(_navigationService)
                        {
                            CodBanner = s.CodBanner,
                            url = s.url,
                            Activo = s.Activo,
                            ImagenMenu = s.ImagenMenu,
                            Nombre = s.Nombre,
                            Tipo = s.Tipo,
                            Acompanamiento = s.Acompanamiento,
                            Dia = s.Dia,
                            Semana = s.Semana,
                            url2 = s.url2,
                            Calorias = s.Calorias
                        })
                    .OrderBy(o=>o.Dia)
                    .ToList());

                PlatillosChef = new ObservableCollection<PlatilloItemViewModel>(
                    _data.Chef.Select(
                        s => new PlatilloItemViewModel(_navigationService)
                        {
                            CodBanner = s.CodBanner,
                            url = s.url,
                            Activo = s.Activo,
                            ImagenMenu = s.ImagenMenu,
                            Nombre = s.Nombre,
                            Tipo = s.Tipo,
                            Acompanamiento = s.Acompanamiento,
                            Dia = s.Dia,
                            Semana = s.Semana,
                            url2 = s.url2,
                            Calorias = s.Calorias
                        })
                    .ToList());

                PlatillosCombo = new ObservableCollection<PlatilloItemViewModel>(
                    _data.Combo.Select(
                        s => new PlatilloItemViewModel(_navigationService)
                        {
                            CodBanner = s.CodBanner,
                            url = s.url,
                            Activo = s.Activo,
                            ImagenMenu = s.ImagenMenu,
                            Nombre = s.Nombre,
                            Tipo = s.Tipo
                        })
                    .ToList());

                PlatillosReposteria = new ObservableCollection<PlatilloItemViewModel>(
                    _data.Reposteria.Select(
                        s => new PlatilloItemViewModel(_navigationService)
                        {
                            CodBanner = s.CodBanner,
                            url = s.url,
                            Activo = s.Activo,
                            ImagenMenu = s.ImagenMenu,
                            Nombre = s.Nombre,
                            Tipo = s.Tipo
                        })
                    .ToList());

                Publicidad = new Publicidad { 
                     CodPublicidad = _data.Publicidad[0].CodPublicidad,
                     Descripcion = _data.Publicidad[0].Descripcion,
                     Url = _data.Publicidad[0].Url                     
                     //Url = "https://i.pinimg.com/originals/1e/f3/e5/1ef3e5379bc068f0133162de742655b5.gif"
                };

                await Task.Delay(1500);
                IsPlaying = true;
                IsRunning = false;
                IsVisible = true;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }
    }
}
