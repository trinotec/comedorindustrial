﻿using ComedorIndustrial.Prism.Helper;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class FechaMonederoPageViewModel : ViewModelBase
    {
        private DelegateCommand _filtrarCommand;
        private DelegateCommand _cancelarCommand2;
        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;


        private DateTime _fechaInicio;
        public DateTime FechaInicio
        {
            get => _fechaInicio;
            set => SetProperty(ref _fechaInicio, value);
        }

        private DateTime _fechaFin;
        public DateTime FechaFin
        {
            get => _fechaFin;
            set => SetProperty(ref _fechaFin, value);
        }

        public DelegateCommand FiltrarCommand => _filtrarCommand ?? (_filtrarCommand = new DelegateCommand(ExecuteFiltrarCommand));
        public DelegateCommand CerrarCommand => _cancelarCommand2 ?? (_cancelarCommand2 = new DelegateCommand(ExecuteCancelarCommand));
        public FechaMonederoPageViewModel(INavigationService navigationService, IDialogService dialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _dialogService = dialogService;
            FechaInicio = DateTime.Now;
            FechaFin = DateTime.Now;
        }
        public void ExecuteFiltrarCommand()
        {
            if (FechaInicio > FechaFin)
            {
                //_dialogService.DisplayAlertAsync("FECHAS", "Corrija las Fechas", "OK");
                CustomDialog.ShowAlert(_dialogService, "Error", "Verifica las fechas");
                return;
            }


            var mainpage = MonederoPageViewModel.GetInstance();
            mainpage.UpdateFacturas(FechaInicio, FechaFin);
            _navigationService.GoBackAsync(useModalNavigation: true);

        }

        public void ExecuteCancelarCommand()
        {
            _navigationService.GoBackAsync(useModalNavigation: true);
        }
    }
}
