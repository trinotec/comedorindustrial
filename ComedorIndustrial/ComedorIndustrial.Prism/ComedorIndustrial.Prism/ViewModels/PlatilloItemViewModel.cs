﻿using ComedorIndustrial.Prism.Model;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class PlatilloItemViewModel : Comida
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _tablaNutricionCommand;

        public PlatilloItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public DelegateCommand TablaNutricionCommand => _tablaNutricionCommand ?? (_tablaNutricionCommand = new DelegateCommand(TablaNutricion));

        private void TablaNutricion()
        {

        }
    }
}
