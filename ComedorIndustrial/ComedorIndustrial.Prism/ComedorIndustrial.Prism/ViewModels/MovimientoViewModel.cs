﻿using ComedorIndustrial.Prism.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComedorIndustrial.Prism.ViewModels
{
    public class MovimientoViewModel :Movimiento
    {
        public string DescripcionMovimiento {
            get
            {
                switch ( TipoTransaccion)
                {
                    case 1:
                        return "Recarga";
                    case 2:
                        return "Consumo";
                        
                    default:
                        return "Default";
                }
                
            }                
        }


        public string MontoFormato
        {
            get
            {
                if (Monto > 0)
                {

                    return "+" + String.Format("{0:N}", Monto);
                }
                else
                {
                    return  String.Format("{0:N}", Monto);
                }

            }
            //string formattedMoneyValue = String.Format("{0:C}", decimalMoneyValue);
        }
 

    }
}
